FROM ubuntu

WORKDIR /usr/src/boompow
RUN \
      apt update && \
      apt -y install --fix-missing \
        build-essential \
        python3 \
        python3-pip \
        ocl-icd-libopencl1 \
        unzip \
        wget \
      && \
      apt clean

RUN wget -nc -O bpow-client.zip https://github.com/BananoCoin/boompow/releases/download/v2.0.3/bpow-client-v203.zip
RUN unzip bpow-client.zip && rm bpow-client.zip

WORKDIR /usr/src/boompow/bpow-client
RUN pip3 install -r requirements.txt

RUN useradd nonroot --uid 2077
RUN chown -R 2077:2077 /usr/src/boompow/bpow-client
USER 2077
