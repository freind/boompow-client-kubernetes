# BoomPow Client Kubernetes

Docker image and Kubernetes Deployment YAML for running a [BANANO BoomPow client](https://github.com/bananocoin/boompow)

## Pulling the Image
To avoid DockerHub rate limiting, the image is published to Gitlab Container Registry

```
docker pull registry.gitlab.com/freind/boompow-client-kubernetes:latest
```

## Installation
1. Update the [BANANO payout address](https://gitlab.com/freind/boompow-client-kubernetes/-/blob/master/bpow-client-deployment.yaml#L41) env var with your own
2. Modify the deployment however you'd like to suit your needs. Feel free to
   give the `boompow-work-server` more cores via the `-c` arg and to tweak the
   container requests and limits
3. Apply the yaml

```
kubectl apply -f bpow-client-deployment.yaml
```
